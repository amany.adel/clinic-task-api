const express = require('express');
require("../models/userModel.js");
require("../models/requestModel.js");





// ====================== all Requests for doctor ======================//
exports.allRequestforDoctor = async (req, res, next) => {
   
    try{
        const allRequestForDoctor = await Request.find({'isAccepted':'yes'});
        

        res.status(201).send({allRequestForDoctor});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};



// ====================== get history of employee ======================//
exports.historyOfEmployee = async (req, res, next) => {
    console.log(req.params.empolyeeId);
     try{
         const historyOfEmployee = await Request.find({'empolyeeId':req.params.empolyeeId,'isAccepted':'done'});
         
 
         res.status(201).send({historyOfEmployee});
     }catch(e){
         res.status(400).send(e);
     }
     
     
 };


// ====================== report on request ======================//
exports.reportOnRequest = async (req, res, next) => {
     try{
         const reportOnRequest = await Request.findOneAndUpdate({'_id':req.body.requestId}, 
         {'isAccepted':'done','reportOfDoctor':req.body.reportDoctor,'doctorId':req.user._id});;
         
 
         res.status(201).send({reportOnRequest});
     }catch(e){
         res.status(400).send(e);
         console.log(e);
     }
     
     
 };


 
// ====================== all Requests for doctor Socket ======================//
exports.allRequest = async (req, res, next) => {
   
    try{
        const allRequestForDoctor = await Request.find({'isAccepted':'yes'});
        

        return {allRequestForDoctor};
    }catch(e){
        console.log(e);
       return e;
    }
    
    
};

