const express = require('express');
require("../models/userModel.js");
require("../models/requestModel.js");




// ====================== request date ======================//
exports.book = async (req, res, next) => {
    const newRequest = new Request({
        empolyeeId:req.user._id,
        report:req.body.report,
        dateRequest:req.body.dateRequest
    });
    try{
        await newRequest.save();
        

        res.status(201).send({newRequest});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};


// ====================== all Requests for employee ======================//
exports.allRequest = async (req, res, next) => {
    console.log('employee is '+req.user);
     try{
         const allRequest = await Request.find({ 'empolyeeId': req.user._id });
         
 
         res.status(201).send({allRequest});
     }catch(e){
         res.status(400).send(e);
     }
     
     
 };

// ====================== all Requests for employee Socket ======================//
exports.allRequestEmployeeSocket = async (employeeId) => {
     try{
         const allRequest = await Request.find({ 'empolyeeId': employeeId }).sort({"createdAt":-1});
         
 
         return  {allRequest };
     }catch(e){
         console.log(e);
         return (e);
     }
     
     
 };





