const express = require('express');
require("../models/userModel.js");
require("../models/requestModel.js");


// ====================== all Requests for Manager ======================//
exports.allRequestforManager = async (req, res, next) => {
   
    try{
        const allRequest = await Request.find({});
        

        res.status(201).send({allRequest});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};

// ====================== accept or ignore request ======================//
exports.statusOfRequest = async (req, res, next) => {
    
     try{
         const reportOnRequest = await Request.findOneAndUpdate({'_id':req.body.requestId}, 
         {'isAccepted':req.body.isAccepted,'managerId':req.user._id});;
         
 
         res.status(201).send({reportOnRequest});
     }catch(e){
         res.status(400).send(e);
     }
     
     
 };



// ====================== get all requests ======================//

 module.exports.getAllRequest = async ()=>{
    try{
        const allRequest = await Request.find({});
        

        return {allRequest};
    }catch(e){
        console.log(e);
         return e;
    }
 };

