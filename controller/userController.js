const express = require('express');
require("../models/userModel.js");
require("../models/requestModel.js");


// ====================== sign up ======================//
exports.signUp = async (req, res, next) => {
    const newUser = new User({
        email:req.body.email,
        password:req.body.password,
        type:req.body.type
    });
    try{
        await newUser.save();
        const token = await newUser.generateAuthToken();

        res.status(201).send({newUser,token});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};

// ====================== log in  ======================//
 exports.logIn = async (req,res,next)=>{
    try{
        console.log('in login fun');
        const user = await User.findByCredentials(req.body.email , req.body.password);
        
        const token = await user.generateAuthToken();

        res.send({user,token});
        
    }catch(e){
        res.status(400).send(e);
        console.log(e);
    }
}

// ====================== log out ======================//
 exports.logOut = async (req,res,next)=>{
    try{
        
        console.log('in logout fun');
        req.user.tokens = req.user.tokens.filter( (token) =>{
            return token.token !== req.token 

        })
        await req.user.save()
        res.send();
        
    }catch(e){
        res.status(500).send();
        console.log(e);
    }
}

// ====================== log out from all devices ======================//
 exports.logOutAll = async (req,res,next)=>{
    try{
        req.user.tokens = [];
        await req.user.save()
        res.send();
        
    }catch(e){
        res.status(500).send();
        console.log(e);
    }
}


// ====================== request date ======================//
exports.book = async (req, res, next) => {
    const newRequest = new Request({
        empolyeeId:req.employee._id,
        report:req.body.report,
        dateRequest:req.body.dateRequest
    });
    try{
        await newRequest.save();
        

        res.status(201).send({newRequest});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};


// ====================== all Requests for User ======================//
exports.allRequest = async (req, res, next) => {
   console.log('employee is '+req.employee);
    try{
        const allRequest = await Request.find({ 'empolyeeId': req.employee._id });
        

        res.status(201).send({allRequest});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};

// ====================== all Requests for Manager ======================//
exports.allRequestforManager = async (req, res, next) => {
   
    try{
        const allRequest = await Request.find({});
        

        res.status(201).send({allRequest});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};

// ====================== all Requests for doctor ======================//
exports.allRequestforDoctor = async (req, res, next) => {
   
    try{
        const allRequestForDoctor = await Request.find({'isAccepted':'pending'});
        

        res.status(201).send({allRequestForDoctor});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};


// ====================== get history of employee ======================//
exports.historyOfEmployee = async (req, res, next) => {
   console.log(req.params.empolyeeId);
    try{
        const historyOfEmployee = await Request.find({'empolyeeId':req.params.empolyeeId,'isAccepted':'done'});
        

        res.status(201).send({historyOfEmployee});
    }catch(e){
        res.status(400).send(e);
    }
    
    
};

// ====================== report on request ======================//
exports.reportOnRequest = async (req, res, next) => {
    console.log(req.params.empolyeeId);
     try{
         const reportOnRequest = await Request.findOneAndUpdate({'_id':req.body.requestId}, 
         {'isAccepted':'done','reportOfDoctor':req.body.reportDoctor,'doctorId':req.doctor._id});;
         
 
         res.status(201).send({reportOnRequest});
     }catch(e){
         res.status(400).send(e);
     }
     
     
 };