const express = require("express");
const app = new express();
const mongoose = require("mongoose");

const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
// const auth = require('./middlewares/auth');
var path = require('path');
const cors = require("cors");
const bodyParser = require("body-parser");
const validators = require('mongoose-validators');


const server = require("http").createServer(app);
const socket = require("socket.io");
const io = socket.listen(server);





// var DateOnly = require('mongoose-dateonly')(mongoose);
var dateFormat = require('dateformat');

require('./dbConnection/db.js');
// require("./models/quizModel");
require("./models/userModel.js");
// const quizRouter = require('./routers/quizRouters')
const userRouter = require('./routes/userRoute.js')
const managerRouter = require('./routes/managerRoute.js')
const doctorRouter = require('./routes/doctorRoute.js')
const employeeRouter = require('./routes/employeeRoute.js')


const managerController = require('./controller/managerController.js');
const doctorController = require('./controller/doctorController.js');
const employeeController = require('./controller/employeeController.js');




app.use(bodyParser.json({ type: "application/*+json" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(bodyParser.raw({ type: "application/vnd.custom-type" }));
app.use(bodyParser.text({ type: "text/html" }));
app.use(cors());

app.use(userRouter);
app.use('/manager', managerRouter);
app.use('/doctor', doctorRouter);
app.use('/employee', employeeRouter);


server.listen(8800, function () {
  console.log("Express server listening on port 8800");


});

io.on("connection", function (client) {
  console.log("client connected ");

  client.on("allRequest", async () => {
    requests = await managerController.getAllRequest();
    io.emit("allRequest", requests);
  });

  client.on("allRequestForDoctor", async () => {
    requests = await doctorController.allRequest();
    io.emit("allRequestForDoctor", requests);
  });

  client.on("allRequestForEmployee", async employeeId => {
    requests = await employeeController.allRequestEmployeeSocket(employeeId);
    io.emit("allRequestForEmployee", requests);
  });

 
  
});


  // var io = require('socket.io')(server);
  // io.on('connection', function(client){
  //   console.log("Client Connected...");
  //   client.on('chat message', function(msg){
  //     console.log('msg is ' + msg);
  //     io.emit('chat message', "Server: You sent:"+ msg);
  //   });
  // });

//   date=dateFormat(Date.now(), "isoDateTime");
//   console.log(date);

// app.get('/add-product',(req,res)=>
// {
//   res.render('add-product');
// })

