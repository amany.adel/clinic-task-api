const mongoose = require("mongoose");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
// var DateOnly = require('mongoose-dateonly')(mongoose);
var dateFormat = require('dateformat');

Schema = mongoose.Schema;

requestSchema = new Schema(
  {
    empolyeeId: {
      type:Schema.Types.ObjectId,
      ref:'users',
      
    },

    report: {
      type: String,
      required: true
    },
    
    dateRequest
    : {
      type: Date,
      required: true,
      min: dateFormat(Date.now(), "isoDateTime"),
    },
    
    
    isAccepted: {
      type: String,
      enum:['yes','no','pending','done'],
      default: 'pending'
    }
    ,
    managerId: {
        type:Schema.Types.ObjectId,
        ref:'users',
        
      }
    ,
    doctorId: {
        type:Schema.Types.ObjectId,
        ref:'users',
        
    }
    ,
    reportOfDoctor: {
        type: String
        
      }  
  },
  { timestamps: true }
);



Request = mongoose.model("request", requestSchema);