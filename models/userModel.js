const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const validator = require('validator');




UserSchema = new Schema({
    email:{
        type:String,
        lowercase: true,
        required:true,
        unique:true,
        validate(value){
            if(!validator.isEmail(value)){
               throw new Error('email not valid') ;
            }
        }
    },
    password:{
        type:String,
        required:true,
    },
    type:{
        type:String
    },
    tokens :[{
        token:{
            type:String,
            required:true
        }
        
    }]
    

},{timestamps:true})





//============= for tokens ===========================//
UserSchema.methods.generateAuthToken = async function () {
    
    const user =this ;
    const token = await jwt.sign({_id: user._id.toString()},'privateTokenKey');

    user.tokens = await user.tokens.concat({token})
    await user.save(); 
    return token
}



//=============== for hash password ===============//
UserSchema.pre('save',async function(next){
    const user =this 
    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password,12)
    }
    next();

})




//============== for login =======================///
UserSchema.statics.findByCredentials = async(email,password)=>{
    const user = await User.findOne({ email }); 
    console.log("user is "+user)
    if(!user) {
        throw new Error('unable to login')
    }
          
    const isMatch = await bcrypt.compare( password ,user.password);
    
    if(!isMatch){
        throw new Error('unable to login ')
    }
    return user
    
}

User = mongoose.model('users',UserSchema);