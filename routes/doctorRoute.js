const express = require('express');
const router = new express.Router();


// const authDoctor = require('../middlwares/authDoctor.js');
const authUser = require('../middlwares/authUser.js');

const doctorController = require('../controller/doctorController.js');


//================== show all request for doctor ===========================//

router.post('/allRequestforDoctor',authUser,doctorController.allRequestforDoctor);

//================== make report on request ===========================//

router.post('/reportOnRequest',authUser,doctorController.reportOnRequest);

//================== show history employee ===========================//

router.get('/historyOfEmployee/:empolyeeId',authUser,doctorController.historyOfEmployee);






module.exports = router