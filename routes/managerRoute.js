const express = require('express');
const router = new express.Router();

// const authManager = require('../middlwares/authManager.js');
const authUser = require('../middlwares/authUser.js');

const managerController = require('../controller/managerController.js');



//================== show all requests for manager ===========================//

router.post('/allRequestforManager',authUser,managerController.allRequestforManager);



//================== accept or ignore request ===========================//

router.post('/statusOfRequest',authUser,managerController.statusOfRequest);




module.exports = router