const express = require('express');
const router = new express.Router();


const userController = require('../controller/userController.js');


//================== sign up ===========================//
router.post('/signup',userController.signUp);

//================== Log in ===========================//

router.post('/login',userController.logIn);

//================== Log Out ===========================//

router.post('/logOut',userController.logOutAll);


module.exports = router